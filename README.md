# Network Time Security Key Exchange Protocol

Network Time Security (NTS) is a development of the venerable Network
Time Protocol (NTP). NTS defines a separate Network Time Security Key
Establishment (NTS-KE) protocol to set up keys and initial cookies.
This is an implementation of NTS-KE in Golang.

This project is a part of an NTS implementation. Other parts:

- [ntskeserver](https://gitlab.com/hacklunch/ntskeserver/) - an NTS-KE
  server using this library.
- [ntp](https://github.com/mchackorg/ntp) NTP/NTS Go library.
- [ntsclient](https://gitlab.com/hacklunch/ntsclient/) - a simple NTS
  client.
